from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem #from folder.file import model class
from todos.forms import TodoListForm, TodoItemForm
# Create your views here. View function 1. gets the data 2. puts data in context
#3.renders the HTML with that context data
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    #every view function has context
    #you can call the context variable, which is the first part, anything you want
    context = {
        "todo_list_object": todo_list,

    }
    return render(request, "todos/list.html", context)#the thing in quotations is the path to the template that we want the context to render to - has the folder / file name
#context is how we forward the data to the html file
#the template only receives the context
#the RENDER takes the three things it's given, combines them, and returns it back to the browser
#we don't need to worry about the request, that's just for Django to keep track of which data goes with which request because in real life, you're going to get lots of requests, so that's how Django keeps track
def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list1": todo,

    }
    return render(request, "todos/detail.html", context)

#create post function below
def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST) #you can call todolistform anything you want
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoListForm()
    context= {
        "form": form

    }
    return render(request, "todos/create.html", context)

#edit todo list function below
def todo_list_update(request, id):
    edit = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=edit)
        if form.is_valid():
            edit = form.save()
            return redirect("todo_list_detail", edit.id)
    else:
        form = TodoListForm(instance=edit)

    context = {
        "form": form,

    }
    return render(request, "todos/edit.html", context)
        #we redirect to todo_list_detail because that's where we go to see the things we want to edit

def todo_list_delete(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "itemform": form,

    }
    return render(request, "todos/item.html", context)

def todo_item_update(request, id):
    edit = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=edit)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail")
    else:
        form = TodoItemForm(instance=edit)
    context = {
        "form": form
    }
    return render(request, "todos/update.html", context)
